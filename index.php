<?php
include "App/Base.php";
$tes2 = new Db();
$tes2->SetTable("task_mnger");
$Rrsult = $tes2->SelectData();

if (isset($_GET["Delete_task"]) && is_numeric($_GET["Delete_task"])) {
  $tes2->DeleteData($_GET["Delete_task"]);
}

if (isset($_GET["Edit_task"]) && is_numeric($_GET["Edit_task"])) {
  $filds = ['Name_Task', 'Title_Task'];
  $data = ['Name_Task' => $_GET['title'], 'Title_Task' => $_GET['text']];
  /*   $tes2->UpdateData($filds,$data,$_GET["Delete_task"]); */
}

?>
<!DOCTYPE html>
<html>

<head>
  <title>مدیریت وظیفه</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="asstes/css/style.css">
</head>

<body>

  <div class="container">

    <h3>All Todo</h3>
    <ul id="incomplete-tasks">
      <?php foreach ($Rrsult as $value) : ?>
        <a class="Remove" href="?Delete_task=<?php echo $value->id; ?>" onclick="return confirm('Are sure delete ?');">Delete</a>
        <a class="Edit" href="taskJob.php?id=<?php echo $value->id; ?>&&job=edit">Edit</a>
        <h5><?php echo  $value->Name_Task . PHP_EOL; ?></h5>
        <h6><?php echo  $value->Title_Task; ?></h6>

      <?php endforeach; ?>

    </ul>
    <button style="background: #3f00ff; border: 0px; color: #888; font-size: 15px; width: 137px; margin: 10px 0 0; font-family: Lato, sans-serif; cursor: pointer;text-align: right;"><a style="text-decoration: none; color: white;" href="taskJob.php?job=add">اضافه کردن برنامه جدید</a> </button>
  </div>
  <div>

  </div>
  <script type="text/javascript" src="js/app.js"></script>

</body>

</html>